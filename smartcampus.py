#coding:utf-8

import requests
import re

login=input("Login : ")
password=input("Password : ")

# on utilie une session request pour conserver les cookies
s=requests.Session()

# première requête pour récupérer les cookies (miam) ainsi que le token
r=s.get("https://smartcampus.wifirst.net/sessions/new")
# extraction du token
plain=re.search(r'<meta content.*name="csrf-token" \/>',r.text,re.MULTILINE).group(0)
token=plain.replace('<meta content="','').replace('" name="csrf-token" />','')

print("J'ai le token :-P C'est : "+token)

# deuxième requête réalisée avec le token et les identifiants
r2=s.post("https://smartcampus.wifirst.net/sessions", data={'utf8':'✓','authenticity_token':token,'login':login,'password':password},cookies=s.cookies)
#print(r2.text)

# troisième requête intermédiaire pour valider le login
r3=s.get("https://connect.wifirst.net/?perform=true",cookies=s.cookies)
#print(r3.text)

print("J'ai passé le login, encore quelques instants.")

# quatrième requête (la plus complexe) que je ne sais pas trop à quoi elle sert, mais il s'agit de s'identifier une seconde fois grâce à des identifiants constants (ce sont toujours les même, on les recupère avec la requête précédente)
r4=s.post("https://wireless.wifirst.net:8090/goform/HtmlLoginRequest", data={'username':'w/7538197@wifirst.net','password':'j3qa4n3gk7kjdw4q6pn4','qos_class':'0','success_url':'https://apps.wifirst.net/?redirected=true','error_url':'https://connect.wifirst.net/login_error'},cookies=s.cookies,headers={'content-type': 'application/x-www-form-urlencoded'})
#print(r4.request.headers)
#print(r4.status_code)
#print(r4.text)

if('You are now connected to the Internet.' in r4.text):
	print("Ça y est !")
elif('You have exceeded the number of sessions allowed (5 sessions).' in r4.text):
        r5=s.get("https://connect.wifirst.net/?ignore_conflicts=true&amp;reason=Device",cookies=s.cookies)
        #print(r5.text)
        print("J'ai du déconnecter la plus vieille session, relancez le script ;-)")
else:
	print("Ça n'a pas fonctionné, vérifiez votre mot de passe ;-)")
